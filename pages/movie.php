<div id="addLightbox">
	<div id="close">×</div>
	<h2>Ajouter un film:</h2>
	<form name="addLightbox" method="POST">
		<table>
			<tr>
				<td class="w150px">Titre</td>
				<td>
					<input type="text">
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Annee</td>
				<td>
					<input type="text">
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Realisateur</td>
				<td>
					<input type="text">
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
			<tr>
				<td class="w150px">Whatever</td>
				<td>
					<input type="text">
				</td>
				<td >
					<span style='color:red;font-weight:bold;position:relative;top:-4px;left:10px;'></span>
				</td>
			</tr>
		</table>
		<input type="submit" name="submit" class="submit" id="submit" value="Ajouter">
	</form>
</div>

<div id="films">
	<ul>
		<?php displayFilms($db); ?>
	</ul>
</div>